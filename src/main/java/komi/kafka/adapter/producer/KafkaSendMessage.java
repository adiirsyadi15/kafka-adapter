package komi.kafka.adapter.producer;

import komi.kafka.adapter.utils.PropertiesFile;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class KafkaSendMessage {
    PropertiesFile ps;
    public KafkaSendMessage() {
        ps = new PropertiesFile();
    }
    public void send(String topic, Object message){
        String bootstrapServers = ps.getValue("kafka.bootstarp-server");
        // create Producer properties
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "komi.kafka.adapter.consumer.IDataSerializer");
        // create the producer
        KafkaProducer<String, Object> producer = new KafkaProducer<>(properties);
        // create a producer record
        ProducerRecord<String, Object> producerRecord =
                new ProducerRecord<>(topic, message);

        producer.send(producerRecord);
        producer.flush();
        producer.close();
        System.out.println("[KafkaProducer] topic: " +topic+ ", send message : " + message);
    }

}
