package komi.kafka.adapter.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFile {
    public String getValue(String parameter){
        try {
            Properties props = PropertiesFile.load();
            return props.getProperty(parameter);
        }catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public static Properties load() throws IOException {
        Properties props = new Properties();
        FileInputStream fis = new FileInputStream("application.properties");
        props.load(fis);
        fis.close();
        return props;
    }
}
