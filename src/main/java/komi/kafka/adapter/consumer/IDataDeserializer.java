package komi.kafka.adapter.consumer;

import com.wm.app.b2b.server.ServiceException;
import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataFactory;
import com.wm.data.IDataUtil;
import com.wm.util.coder.IDataBinCoder;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;

public class IDataDeserializer implements Deserializer<IData> {
    @Override
    public IData deserialize(String s, byte[] bytes) {
        IData id = IDataFactory.create();
        IDataCursor idc = id.getCursor();
        if (bytes != null) {
            IDataBinCoder coder = new IDataBinCoder();

            try {
                id = coder.decodeFromBytes(bytes);
//                idc.insertAfter("document", doc);
            } catch (IOException var5) {
                System.out.println(var5.getMessage());
            }
        }

        idc.destroy();
        return id;
    }
}
