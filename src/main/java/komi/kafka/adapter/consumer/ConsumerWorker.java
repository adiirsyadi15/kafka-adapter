package komi.kafka.adapter.consumer;

import com.wm.app.b2b.client.Context;
import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataFactory;
import com.wm.data.IDataUtil;
import com.wm.lang.ns.NSName;
import komi.kafka.adapter.utils.PropertiesFile;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class ConsumerWorker implements Runnable{

    private CountDownLatch countDownLatch;
    private KafkaConsumer<String, Object> consumer;

    private String consumerServiceName;
    private String consumerDocument;
    private String topic;
    private String groupId;
    private PropertiesFile prop;


    public ConsumerWorker(String consumerServiceName,String consumerDocument, String topic, String groupId) {
        this.topic = topic;
        this.groupId = groupId;
        this.consumerServiceName = consumerServiceName;
        this.consumerDocument = consumerDocument;
        prop = new PropertiesFile();
    }

    @Override
    public void run() {
        countDownLatch = new CountDownLatch(1);
        String bootstrapServers = prop.getValue("kafka.bootstarp-server");
        String isServer = prop.getValue("kafka.consumer.isserver");
        String username = prop.getValue("kafka.consumer.username");
        String password = prop.getValue("kafka.consumer.password");

        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "komi.kafka.adapter.consumer.IDataDeserializer");
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // we disable Auto Commit of offsets
//        properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

        consumer = new KafkaConsumer<String, Object>(properties);
        consumer.subscribe(Collections.singleton(topic));
        ConsumerRebalanceListenerImpl listener = new ConsumerRebalanceListenerImpl(consumer);

        final Duration pollTimeout = Duration.ofMillis(100);

        try {
            while (true) {
                final ConsumerRecords<String, Object> consumerRecords = consumer.poll(pollTimeout);
                for (final ConsumerRecord<String, Object> consumerRecord : consumerRecords) {
//                    System.out.println("Getting consumer record key: '" + consumerRecord.key() + "', value: '" + consumerRecord.value() + "', partition: " + consumerRecord.partition() + " and offset: " + consumerRecord.offset() + " at " + new Date(consumerRecord.timestamp()));
                    // we track the offset we have been committed in the listener
//                    invoke
                    IData docIs = IDataFactory.create();
                    IDataCursor idc = docIs.getCursor();
                    IDataUtil.put(idc,consumerDocument,consumerRecord.value());
                    Context ctx = new Context();
                    ctx.connect (isServer, username, password);
                    NSName nsName = NSName.create(consumerServiceName);
                    ctx.invoke(nsName, docIs);
                    idc.destroy();
                    System.out.println("[KafkaConsumer] topic:" + topic + ", incoming message " + consumerRecord.value());
                    listener.addOffsetToTrack(consumerRecord.topic(), consumerRecord.partition(), consumerRecord.offset());
                    System.out.println("[KafkaConsumer] commit :" + consumerRecord.topic());
                }
            }
        }catch (WakeupException e) {
            System.out.println("Consumer poll woke up");
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            consumer.close();
            countDownLatch.countDown();
        }
    }

    void shutdown() throws InterruptedException {
        consumer.wakeup();
        countDownLatch.await();
        System.out.println("Consumer closed");
    }

    private void finalState(KafkaConsumer<String, String> consumer,ConsumerRebalanceListenerImpl listener){
        try {
            consumer.commitSync(listener.getCurrentOffsets()); // we must commit the offsets synchronously here
        } finally {
            consumer.close();
            System.out.println("The consumer is now gracefully closed.");
        }
    }
}
