package komi.kafka.adapter.consumer;

public class ConsumerWorkerCloser implements Runnable{

    private final ConsumerWorker consumerDemoWorker;

    public ConsumerWorkerCloser(ConsumerWorker consumerDemoWorker) {
        this.consumerDemoWorker = consumerDemoWorker;
    }

    @Override
    public void run() {
        try {
            consumerDemoWorker.shutdown();
        } catch (InterruptedException e) {
            System.out.println("Error shutting down consumer"+ e);
        }
    }
}
