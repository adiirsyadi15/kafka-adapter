package komi.kafka.adapter.consumer;

import com.wm.data.IData;
import com.wm.util.coder.IDataBinCoder;
import org.apache.kafka.common.serialization.Serializer;

import java.io.IOException;
import java.util.Map;

public class IDataSerializer implements Serializer<IData> {

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
    }

    @Override
    public byte[] serialize(String s, IData iData){
        byte[] bytes = null;
        if (iData != null) {
            IDataBinCoder coder = new IDataBinCoder();
            try {
                bytes = coder.encodeToBytes(iData);
            } catch (IOException var5) {
                System.out.println(var5.getMessage());
            }
        }
        return bytes;
    }
}
