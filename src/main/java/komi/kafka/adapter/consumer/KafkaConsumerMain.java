package komi.kafka.adapter.consumer;

public class KafkaConsumerMain {
    public static void main(String[] args) {
        KafkaReceiveMessage receiveMessage = new KafkaReceiveMessage();
        System.out.println("data from kafka : " + receiveMessage.receive("komi-bifast", "incoming"));
    }
}
