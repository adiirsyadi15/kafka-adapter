package komi.kafka.adapter.consumer;

public class KafkaThreadListener {
    private ConsumerWorker consumerWorker;
    public void listener(String consumerServiceName,String consumerDocument, String topic, String groupId){
        consumerWorker = new ConsumerWorker(consumerServiceName,consumerDocument,topic,groupId);
        new Thread(consumerWorker).start();
    }

    public void shutdownListener(){
        Runtime.getRuntime().addShutdownHook(new Thread(new ConsumerWorkerCloser(consumerWorker)));
    }
}
