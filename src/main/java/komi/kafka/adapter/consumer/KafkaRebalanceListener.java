package komi.kafka.adapter.consumer;

import com.wm.app.b2b.server.Service;
import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataFactory;
import com.wm.data.IDataUtil;
import com.wm.lang.ns.NSName;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;


public class KafkaRebalanceListener {
    public String listener(String consumerServiceName, String topic, String groupId){
        Object value = null;

        String bootstrapServers = "54.66.27.28:9093";
//        String groupId = "my-fifth-application";
//        String topic = "komi-bifast";

        // create consumer configs
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // we disable Auto Commit of offsets
        properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

        // create consumer
        KafkaConsumer<String, Object> consumer = new KafkaConsumer<>(properties);

        ConsumerRebalanceListenerImpl listener = new ConsumerRebalanceListenerImpl(consumer);

        newConsumerThread(consumer);

        try {
            // subscribe consumer to our topic(s)
            consumer.subscribe(Arrays.asList(topic), listener);

            // poll for new data
            while (true) {
                ConsumerRecords<String, Object> records =
                        consumer.poll(Duration.ofMillis(100));

                for (ConsumerRecord<String, Object> record : records) {
                    value = record.value();
                    System.out.println("Key: " + record.key() + ", Value: " + record.value());
                    System.out.println("Partition: " + record.partition() + ", Offset:" + record.offset());

                    // we track the offset we have been committed in the listener
                    listener.addOffsetToTrack(record.topic(), record.partition(), record.offset());
//                    invoke
                    IData docIs = IDataFactory.create();
                    IDataCursor idc = docIs.getCursor();
                    IDataUtil.put(idc,"incoming",record.value());
                    Service.doInvoke("komi.bifast.auditlog.messagingProcessor","logging",docIs);
                    idc.destroy();
                }

                // We commitAsync as we have processed all data and we don't want to block until the next .poll() call
                consumer.commitAsync();
            }
        } catch (WakeupException e) {
            System.out.print("Wake up exception!");
            // we ignore this as this is an expected exception when closing a consumer
        } catch (Exception e) {
            System.out.print("Unexpected exception" + e);
        } finally {
            finalState(consumer,listener);
        }

        return value.toString();

    }

    private void finalState(KafkaConsumer<String, Object> consumer,ConsumerRebalanceListenerImpl listener){
        try {
            consumer.commitSync(listener.getCurrentOffsets()); // we must commit the offsets synchronously here
        } finally {
            consumer.close();
            System.out.println("The consumer is now gracefully closed.");
        }
    }

    private void newConsumerThread(KafkaConsumer<String, Object> consumer){
        // get a reference to the current thread
        final Thread mainThread = Thread.currentThread();
        // adding the shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                System.out.println("Detected a shutdown, let's exit by calling consumer.wakeup()...");
                consumer.wakeup();
                // join the main thread to allow the execution of the code in the main thread
                try {
                    mainThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
